# SPDX-License-Identifier: GPL-2.0-or-later

package Mail::SpamAssassin::Logger::Amavislog;
use strict;
use re 'taint';
use warnings;
use warnings FATAL => qw(utf8 void);
no warnings 'uninitialized';
# use warnings 'extra'; no warnings 'experimental::re_strict'; use re 'strict';

BEGIN {
  require Exporter;
  use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS $VERSION);
  $VERSION = '2.412';
  @ISA = qw(Exporter);
}

use Amavis::Util qw(ll do_log);

sub new {
  my($class,%args) = @_;
  my(%llmap) = (error => -1, warn => 0, info => 1, dbg => 3);
  # $args{debug} is a simple boolean, sets the log level floor to 1 when true
  if ($args{debug}) { for (keys %llmap) { $llmap{$_} = 1 if $llmap{$_} > 1 } }
  bless { llmap => \%llmap }, $class;
}

sub close_log { 1 }

sub log_message {
  my($self, $level,$msg) = @_;
  my $ll = $self->{llmap}->{$level};
  $ll = 1  if !defined $ll;
  ll($ll) && do_log($ll, "SA %s: %s", $level,$msg);
  1;
}

1;
