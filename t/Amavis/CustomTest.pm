# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::CustomTest;

use Test::Most;
use base 'Test::Class';

sub class { 'Amavis::Custom' }

sub startup : Tests(startup => 1) {
  my $test = shift;
  use_ok $test->class;
}

sub hooks : Tests(5) {
    my $class = shift->class;
    can_ok $class, $_ for qw(new checks before_send after_send mail_done);
}

1;
